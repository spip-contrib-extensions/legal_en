<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'legal_en_description' => 'Aide à la rédaction des pages "Mentions légales" et "Données personnelles et cookies".',
	'legal_en_slogan' => 'Plugin dédié aux sites de l\'Education Nationale utilisant le squelette Escal.',
);