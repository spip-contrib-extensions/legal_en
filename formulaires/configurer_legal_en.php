<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_legal_en_saisies_dist(){

	$saisies = array(
			'options' => array(
				'inserer_debut' => '
				<div style="padding:10px;background:var(--spip-color-theme-lighter,#BBCCDD);margin: 5px;border-radius: 5px;">
				<strong>Attention : </strong>
				Tous les champs obligatoires des 4 premiers onglets doivent être renseignés pour pouvoir enregistrer le formulaire.
				</div>'
				),


// fieldset editeur
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_editeur',
				'label' => 'Editeur',
				'onglet' => 'oui',
				),
		'saisies' => array(
					array(
						'saisie' => 'selection',
						'options' => array(
							'nom' => 'academie',
							'label' => 'Votre académie',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							'data' => array(
								'Académie d’Aix-Marseille' => 'Aix-Marseille',
								'Académie d’Amiens' => 'Amiens',
								'Académie de Besançon' => 'Besançon',
								'Académie de Bordeaux' => 'Bordeaux',
								'Académie de Clermont' => 'Clermont-Ferrand',
								'Académie de Corse' => 'Corse',
								'Académie de Créteil' => 'Créteil',
								'Académie de Dijon' => 'Dijon',
								'Académie de Grenoble' => 'Grenoble',
								'Académie de Guadeloupe' => 'Guadeloupe',
								'Académie de Guyane' => 'Guyane',
								'Académie de La Réunion' => 'La Réunion',
								'Académie de Lille' => 'Lille',
								'Académie de Limoges' => 'Limoges',
								'Académie de Lyon' => 'Lyon',
								'Académie de Martinique' => 'Martinique',
								'Académie de Mayotte' => 'Mayotte',
								'Académie de Montpellier' => 'Montpellier',
								'Académie de Nancy-Metz' => 'Nancy-Metz',
								'Académie de Nantes' => 'Nantes',
								'Académie de Nice' => 'Nice',
								'Académie de Nouvelle-Calédonie' => 'Nouvelle-Calédonie',
								'Académie de Normandie' => 'Normandie',
								'Académie de Orléans-Tours' => 'Orléans-Tours',
								'Académie de Paris' => 'Paris',
								'Académie de Poitiers' => 'Poitiers',
								'Académie de Polynésie française' => 'Polynésie française',
								'Académie de Reims' => 'Reims',
								'Académie de Rennes' => 'Rennes',
								'Académie de Saint-Pierre-et-Miquelon' => 'Saint-Pierre-et-Miquelon',
								'Académie de Strasbourg' => 'Strasbourg',
								'Académie de Toulouse' => 'Toulouse',
								'Académie de Versailles' => 'Versailles',
								'Académie de Wallis-et-Futuna' => 'Wallis-et-Futuna',
								),
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'denomination',
							'label' => 'Dénomination de l’éditeur',
							'explication' => 'Rectorat, DSDEN, CIO, lycée, collège ou école',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'adresse_postale',
							'label' => 'Adresse postale',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'code_postal',
							'label' => 'Code postal',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'ville',
							'label' => 'Commune',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'tel',
							'label' => 'Numéro de téléphone',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'radio',
						'options' => array(
							'nom' => 'structure',
							'label' => 'Votre site concerne',
							'data' => array(
								'general' => 'une discipline d’enseignement général',
								'technique' => 'une discipline d’enseignement technique',
								'cio' => 'un CIO',
								'circo' => 'une circonscription',
								'ecole' => 'une école',
								'degre2' => 'un collège ou un lycée',
								'autre' => 'autre',
								),
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
					array(
						'saisie' => 'radio',
						'options' => array(
							'nom' => 'departement',
							'label' => 'Département',
							'data' => array(
								'de l’Ain' => 'Ain',
								'de la Loire' => 'Loire',
								'du Rhone' => 'Rhone',
								),
							'afficher_si' => '@structure@=="circo"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_cio',
							'label' => 'Nom du CIO',
							'afficher_si' => '@structure@=="cio"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_ecole',
							'label' => 'Nom de l’école',
							'afficher_si' => '@structure@=="ecole"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_etab',
							'label' => 'Nom de l’établissement',
							'afficher_si' => '@structure@=="degre2"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_autre',
							'label' => 'Nom de la structure',
							'afficher_si' => '@structure@=="autre"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							),
					),
				)
			),// fin du fieldset editeur

// fieldset dirpub
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_dirpub',
				'label' => 'Directeur de publication',
				'explication' => 'L’IA-IPR, IEN-ET-EG pour un site disciplinaire<br>L’IA-DASEN du département pour un site de la DSDEN<br>Le chef d’établissement pour un site d’établissement scolaire',
				'onglet' => 'oui',
				),
		'saisies' => array(
					array(
						'saisie' => 'radio',
						'options' => array(
							'nom' => 'civ_dirpub',
							'label' => 'Civilité',
							'obligatoire' => 'oui',
							'data' => array(
								'Mme' => 'Madame',
								'M.' => 'Monsieur',
								)
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_dirpub',
							'label' => 'Nom',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'prenom_dirpub',
							'label' => 'Prénom',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'fonction_autre',
							'label' => 'Fonction',
							'afficher_si' => '@structure@=="autre"',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
				)
			),// fin du fieldset dirpub

// fieldset webmestre
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_webmestre',
				'label' => 'Webmestre',
				'onglet' => 'oui',
				),
		'saisies' => array(
					array(
						'saisie' => 'radio',
						'options' => array(
							'nom' => 'civ_webmestre',
							'label' => 'Civilité',
							'obligatoire' => 'oui',
							'data' => array(
								'Mme' => 'Madame',
								'M.' => 'Monsieur',
								)
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'nom_webmestre',
							'label' => 'Nom',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'prenom_webmestre',
							'label' => 'Prénom',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'fonction_webmestre',
							'label' => 'Fonction',
							'explication' => 'Enseignant, secrétaire, CPE, ...',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),

				)
			),// fin du fieldset webmestre

// fieldset dpd
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_dpd',
				'label' => 'DPD',
				'onglet' => 'oui',
				),
		'saisies' => array(
					array(
						'saisie' => 'explication',
						'options' => array(
							'nom' => 'explic_dpd',
							'texte' => 'DPD : Délégué à la Protection des Données',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'mail_dpd',
							'label' => 'Adresse mail du DPD',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
						),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'adresse_postale_dpd',
							'label' => 'Adresse postale du Rectorat',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'code_postal_dpd',
							'label' => 'Code postal du Rectorat',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'ville_dpd',
							'label' => 'Commune du Rectorat',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire' => 'oui',
							)
					),
				)
			),// fin du fieldset dpd

// fieldset credits
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_credits',
				'label' => 'Crédits',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'explication',
					'options' => array(
						'nom' => 'explic_credits',
						'texte' => 'Indiquer ici la ou les sources de vos photos en précisant si possible la licence d’utilisation<br><br>
						<strong>Exemple :</strong><br>
						https://pixabay.com/fr/<br>
						Pixabay License (libre pour usage commercial – pas d’attribution requise)',
						)
					),

// fieldset source1
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_source1',
				'label' => 'Source 1',
				),
		'saisies' => array(
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'photo1',
							'label' => 'Source',
							)
						),
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'licencephoto1',
							'label' => 'Licence',
							)
						),
				)
			),// fin du fieldset source1
// fieldset source2
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_source2',
				'label' => 'Source 2',
				),
		'saisies' => array(
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'photo2',
							'label' => 'Source',
							)
						),
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'licencephoto2',
							'label' => 'Licence',
							)
						),
				)
			),// fin du fieldset source2
// fieldset source3
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_source3',
				'label' => 'Source 3',
				),
		'saisies' => array(
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'photo3',
							'label' => 'Source',
							)
						),
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'licencephoto3',
							'label' => 'Licence',
							)
						),
				)
			),// fin du fieldset source3
// fieldset source4
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_source4',
				'label' => 'Source 4',
				),
		'saisies' => array(
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'photo4',
							'label' => 'Source',
							)
						),
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'licencephoto4',
							'label' => 'Licence',
							)
						),
				)
			),// fin du fieldset source4
// fieldset source5
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_source5',
				'label' => 'Source 5',
				),
		'saisies' => array(
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'photo5',
							'label' => 'Source',
							)
						),
						array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'licencephoto5',
							'label' => 'Licence',
							)
						),
				)
			),// fin du fieldset source5


				)
			),// fin du fieldset credits

// un fieldset
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fieldset_cookies',
				'label' => 'Cookies',
				'onglet' => 'oui',
				),
		'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'cookies',
						'label' => 'Votre site utilise-t-il un outil de mesures d’audience ?',
						'conteneur_class' => 'pleine_largeur',
						'defaut' => 'non',
						'data' => array(
							'non' => '<:item_non:>',
							'oui' => '<:item_oui:>',
							)
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'outil_cookie',
						'label' => 'Nom de l’outil utilisé',
						'afficher_si' => '@cookies@=="oui"',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'nom_cookie',
						'label' => 'Nom du cookie',
						'afficher_si' => '@cookies@=="oui"',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'finalite_cookie',
						'label' => 'Finalité du cookie',
						'afficher_si' => '@cookies@=="oui"',
						)
					),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'duree_cookie',
						'label' => 'Durée de conservation',
						'afficher_si' => '@cookies@=="oui"',
						)
					),
				)
			),// fin du fieldset

	);
	return $saisies;
}