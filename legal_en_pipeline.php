<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function legal_en_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('legal_en.css') . '" type="text/css" media="all" />';
	return $flux;
}


